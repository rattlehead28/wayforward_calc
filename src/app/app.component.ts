import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  num1 = '';
  num2 = '';
  result: number = 0;
  numArr = ['7', '8', '9', '4', '5', '6', '1', '2', '3', 'C', '0', '='];
  opArr = ['+', '-', '/', '*'];
  toggleSelect: boolean = false;
  selectedOp;

  selectNo(num) {
    if (num == 'C') {
      this.clear();
    } else if (num == '=') {
      this.calResult();
    } else {
      if (!this.toggleSelect) {
        this.result = 0;
        this.num1 += num;
      } else {
        this.num2 += num;
      }
    }
  }

  opSelected(op) {
    this.toggleSelect = true;
    this.selectedOp = op;
  }

  clear() {
    this.result = 0;
    this.num1 = '';
    this.num2 = '';
    this.toggleSelect = false;
    this.selectedOp = '';
  }

  calResult() {
    const num1 = parseInt(this.num1);
    const num2 = parseInt(this.num2);
    switch (this.selectedOp) {
      case this.opArr[0]:
        this.result = num1 + num2;
        break;
      case this.opArr[1]:
        this.result = num1 - num2;
        break;
      case this.opArr[2]:
        if (num2 == 0) {
          alert('Cannot divide by 0');
        } else {
          this.result = num1 / num2;
        }

        break;
      case this.opArr[3]:
        this.result = num1 * num2;
        break;
    }
    this.num1 = '';
    this.num2 = '';
    this.toggleSelect = false;
    this.selectedOp = '';
  }
}
